# Lost In Translation

This is my submission to assignment 3 of .Net Fullstack Upskill - Winter 2021 - Oslo.

It is a website that translates text into one hand sign language.

## Project setup
To view the page in your browser, follow these steps:

- Clone the repository
- `cd lost-in-translation/server`
- `npm install` and `npm start`
- `cd ../client/`
- `npm install` and `npm start`

The page should now open in your browser and be fully functional.
