import { Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../store/actions/loginActions";

import Header from "../../components/header/Header";
import StartMessage from "../../components/startMessage/StartMessage";
import Card from "../../components/card/Card";
import InputBox from "../../components/inputBox/InputBox";

import "./Login.scss";

function Login() {
    const dispatch = useDispatch();
    const { username } = useSelector((state) => state.profileReducer);

    const onLoginClicked = async (username) => {
        dispatch(loginAction(username));
    };

    return (
        <>
            {username && <Redirect to="/" />}
            <Header />
            <div id="login-screen">
                <StartMessage />
                <Card
                    body={
                        <InputBox
                            placeholder="What's your name?"
                            onButtonClick={onLoginClicked}
                        />
                    }
                />
            </div>
        </>
    );
}
export default Login;
