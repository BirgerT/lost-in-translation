const BASE_URL = "http://localhost:5000";

export async function fetchUser(username) {
    const response = await fetch(`${BASE_URL}/users`);
    const users = await response.json();
    return users.find((user) => user.username === username);
}

export async function createUser(username) {
    const response = await fetch(`${BASE_URL}/users`, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({ username, translations: [] }),
    });
    return await response.json();
}
