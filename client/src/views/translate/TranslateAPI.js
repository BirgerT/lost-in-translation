const BASE_URL = "http://localhost:5000";

export async function fetchTranslations(userId) {
    const response = await fetch(`${BASE_URL}/users/${userId}`);
    const translations = await response.json();
    return translations.translations;
}

export async function storeTranslations(userId, translations) {
    const response = await fetch(`${BASE_URL}/users/${userId}`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ translations }),
    });
    const r = await response.json();
    return r.data;
}
