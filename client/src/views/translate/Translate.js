import { useState } from "react";
import { useDispatch } from "react-redux";
import { translationStoreAction } from "../../store/actions/translationActions";

import withAuth from "../../utils/withAuth";

import Header from "../../components/header/Header";
import InputBox from "../../components/inputBox/InputBox";
import TranslationCard from "../../components/translationCard/TranslationCard";

import "./Translate.scss";

function Translate() {
    const dispatch = useDispatch();
    const [translateText, setTranslateText] = useState("");

    const onTranslateClicked = (text) => {
        setTranslateText(text);
        dispatch(translationStoreAction(text));
    };

    return (
        <>
            <Header />
            <div id="translate--body">
                <div id="translate--content" className="page-width">
                    <InputBox
                        placeholder="Type something..."
                        whiteBorder
                        onButtonClick={onTranslateClicked}
                    />
                    {translateText && <TranslationCard text={translateText} />}
                </div>
            </div>
        </>
    );
}

export default withAuth(Translate);
