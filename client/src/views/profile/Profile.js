import { useDispatch, useSelector } from "react-redux";
import { translationClearAction } from "../../store/actions/translationActions";

import { generateId } from "../../utils/idGenerator";
import withAuth from "../../utils/withAuth";

import Header from "../../components/header/Header";
import TranslationCard from "../../components/translationCard/TranslationCard";

import "./Profile.scss";

function Profile() {
    const dispatch = useDispatch();
    const { translations } = useSelector((state) => state.translationReducer);

    const onClearClicked = () => {
        dispatch(translationClearAction());
    };

    return (
        <>
            <Header />
            <div className="page-width">
                <div className="profile--translations-header">
                    Your latest translations:
                </div>
                <button onClick={onClearClicked}>Clear translations</button>
                {translations.filter(trans => trans.deleted === false).map((translation) => (
                    <TranslationCard key={generateId()} text={translation.text} />
                ))}
            </div>
        </>
    );
}
export default withAuth(Profile);
