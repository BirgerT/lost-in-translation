import "./LetterSign.scss";

function LetterSign({ letter }) {
    if (/[A-z]/.test(letter)) {
        const image = require(`./signs/${letter.toLowerCase()}.png`);
        return (
            <img
                className="letter-sign--letter"
                src={image.default}
                alt="letter"
            />
        );
    } else {
        return <span className="letter-sign--punctuation">{letter}</span>;
    }
}
export default LetterSign;
