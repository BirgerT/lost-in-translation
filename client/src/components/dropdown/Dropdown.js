import "./Dropdown.scss";

function Dropdown({ visible = false, items }) {
    return <div className="dropdown--container">{items}</div>;
}
export default Dropdown;
