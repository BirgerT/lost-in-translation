import "./Dropdown.scss";

function DropdownItem({ text = "Item", onItemClick, slot }) {
    return (
        <p className="dropdown--item" onClick={onItemClick}>
            {text}
        </p>
    );
}
export default DropdownItem;
