import './Label.scss';

function Label({ text }) {
    return <span className="label">{text}</span>;
}
export default Label;
