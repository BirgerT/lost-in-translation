import "./StartMessage.scss";
import Logo from "../logo/Logo";

function StartMessage() {
    return (
        <div id="start-message--container">
            <Logo />
            <div id="start-message--text-wrapper">
                <h1 id="start-message--top-text">Lost in Translation</h1>
                <h2 id="start-message--bottom-text">Get started</h2>
            </div>
        </div>
    );
}
export default StartMessage;
