import { useEffect, useState } from "react";
import { generateId } from "../../utils/idGenerator";

import Card from "../../components/card/Card";
import Label from "../../components/label/Label";
import LetterSign from "../../components/letterSign/LetterSign";

import "./TranslationCard.scss";

function TranslationCard({ text = "" }) {
    const [textSigns, setTextSigns] = useState("");

    useEffect(() => {
        const letterArray = [];
        for (const letter of text.split("")) {
            letterArray.push(<LetterSign letter={letter} key={generateId()} />);
        }
        setTextSigns(letterArray);
    }, [text]);

    return (
        <Card
            className="translation-card--card"
            body={textSigns}
            footer={<Label text={`Translation: ${text}`} />}
        />
    );
}
export default TranslationCard;
