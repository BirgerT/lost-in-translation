import { useState } from "react";
import "./InputBox.scss";

function InputBox({
    placeholder = "Placeholder",
    whiteBorder = false,
    onButtonClick,
}) {
    const [inputText, setInputText] = useState("");

    return (
        <div
            id="inputbox--container"
            className={
                whiteBorder ? "inputbox--white-border" : "inputbox--grey-border"
            }
        >
            <input
                id="inputbox--input"
                type="text"
                placeholder={placeholder}
                onChange={(e) => setInputText(e.target.value)}
            />
            <button
                onClick={() => onButtonClick(inputText)}
                id="inputbox--button"
            />
        </div>
    );
}
export default InputBox;
