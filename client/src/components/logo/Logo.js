import { useState } from "react";

import { ReactComponent as Splash } from "./LogoSplash.svg";
import image from "./LogoImage.png";
import helloImage from "./Logo-Hello.png";

import "./Logo.scss";

function Logo({ small = false }) {
    const [hello, setHello] = useState(false);

    const onLogoClicked = () => {
        setHello(true);
        setTimeout(() => {
            setHello(false);
        }, 2000);
    };

    return (
        <div onClick={onLogoClicked}
            id="logo--container"
            className={small ? "logo--small" : "logo--large"}
        >
            <Splash id="logo--splash" />

            <img id="logo--image" src={hello ? helloImage : image} alt="logo" />
        </div>
    );
}
export default Logo;
