import "./ProfileLogo.scss";

function ProfileLogo({ username, onClick }) {
    return (
        <>
            <p id="profile-logo--username" onClick={onClick}>
                {username}
            </p>
        </>
    );
}
export default ProfileLogo;
