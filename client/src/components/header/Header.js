import { useState } from "react";
import { withRouter } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { profileLogoutAction } from "../../store/actions/profileActions";

import Logo from "../logo/Logo";
import ProfileLogo from "../profileLogo/ProfileLogo";
import Dropdown from "../dropdown/Dropdown";
import DropdownItem from "../dropdown/DropdownItem";

import "./Header.scss";

function Header({ history }) {
    const dispatch = useDispatch();
    const { username } = useSelector((state) => state.profileReducer);

    const [dropdownShown, setDropdownShown] = useState(false);

    const onTitleClick = () => {
        history.push("/")
    }

    const onProfileLogoClicked = () => {
        setDropdownShown(!dropdownShown);
    };

    const onProfileClicked = () => {
        history.push("/profile");
    };

    const onLogoutClicked = () => {
        dispatch(profileLogoutAction());
    };

    return (
        <header id="header--container">
            <div id="header--content" className="page-width">
                <div id="header--content-left">
                    {username && <Logo small />}
                    <p id="header--title" onClick={onTitleClick}>Lost In Translation</p>
                </div>

                <div id="header--content-right">
                    {username && (
                        <ProfileLogo
                            username={username}
                            onClick={onProfileLogoClicked}
                        />
                    )}
                    {dropdownShown && (
                        <Dropdown
                            items={
                                <>
                                    <DropdownItem
                                        text="Profile"
                                        onItemClick={onProfileClicked}
                                    />
                                    <DropdownItem
                                        text="Logout"
                                        onItemClick={onLogoutClicked}
                                    />
                                </>
                            }
                        />
                    )}
                </div>
            </div>
        </header>
    );
}
export default withRouter(Header);
