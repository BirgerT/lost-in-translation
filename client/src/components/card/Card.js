import "./Card.scss";

function Card({ body = "", footer = "", className = "" }) {
    return (
        <div id="card--container" className={className}>
            <div id="card--main">{body}</div>
            <div id="card--footer">{footer}</div>
        </div>
    );
}
export default Card;
