import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const withAuth = (Component) => (props) => {
    const { username } = useSelector((state) => state.profileReducer);

    if (username) {
        return <Component {...props} />;
    } else {
        return <Redirect to="/login" />;
    }
};

export default withAuth;
