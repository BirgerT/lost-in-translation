export function storageSet(key, value) {
    const json = JSON.stringify(value);
    const encodedValue = btoa(json);
    localStorage.setItem(key, encodedValue);
}

export function storageGet(key) {
    const encodedValue = localStorage.getItem(key);

    if (encodedValue) {
        const decodedValue = atob(encodedValue);
        return JSON.parse(decodedValue);
    } else {
        return null;
    }
}

export function storageRemove(key) {
    localStorage.removeItem(key);
}
