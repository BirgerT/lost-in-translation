import { applyMiddleware, createStore } from "redux";
import { rootReducers } from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import { loginMiddleware } from "./middleware/loginMiddleware";
import { profileMiddleware } from "./middleware/profileMiddleware";
import { translationMiddleware } from "./middleware/translationMiddleware";

export const store = createStore(
    rootReducers,
    composeWithDevTools(
        applyMiddleware(
            loginMiddleware,
            profileMiddleware,
            translationMiddleware
        )
    )
);
