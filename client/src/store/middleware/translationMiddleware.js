import {
    ACTION_TRANSLATION_FETCH,
    ACTION_TRANSLATION_STORE,
    ACTION_TRANSLATION_CLEAR,
    translationSetAction,
} from "../actions/translationActions";
import {
    fetchTranslations,
    storeTranslations,
} from "../../views/translate/TranslateAPI";

export const translationMiddleware = ({ getState, dispatch }) => (next) => (
    action
) => {
    next(action);

    const userId = getState().profileReducer.id;

    if (action.type === ACTION_TRANSLATION_FETCH) {
        (async () => {
            const translations = await fetchTranslations(userId);
            dispatch(translationSetAction(translations));
        })();
    }

    if (action.type === ACTION_TRANSLATION_STORE) {
        const translations = getState().translationReducer.translations;
        translations.unshift({ text: action.payload, deleted: false });

        while (translations.length > 10) {  // Hello magic number!
            translations.pop();
        }
        storeTranslations(userId, translations);
        dispatch(translationSetAction(translations));
    }

    if (action.type === ACTION_TRANSLATION_CLEAR) {
        const translations = getState().translationReducer.translations;
        const deleted = translations.map((trans) => ({ text: trans.text, deleted: true }));

        storeTranslations(userId, deleted);
        dispatch(translationSetAction(deleted));
    }
};
