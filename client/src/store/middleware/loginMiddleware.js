import {
    ACTION_LOGIN,
    ACTION_LOGIN_SUCCESS,
    loginSuccessAction,
    loginErrorAction,
} from "../actions/loginActions";
import {
    profileSetAction,
    profileCreateAction,
} from "../actions/profileActions";
import { fetchUser } from "../../views/login/LoginAPI";

export const loginMiddleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (action.type === ACTION_LOGIN) {
        (async () => {
            const user = await fetchUser(action.payload);
            if (user) {
                dispatch(loginSuccessAction(user));
            } else {
                dispatch(loginErrorAction("Failed login"));
                dispatch(profileCreateAction(action.payload));
            }
        })();
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(profileSetAction(action.payload));
    }
};
