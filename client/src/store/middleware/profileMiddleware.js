import {
    ACTION_PROFILE_CHECK_EXISTING,
    ACTION_PROFILE_SET,
    ACTION_PROFILE_CREATE,
    ACTION_PROFILE_LOGOUT,
    profileSetAction,
    profileErrorAction,
} from "../actions/profileActions";
import { loginAction } from "../actions/loginActions";
import {
    translationFetchAction,
    translationSetAction,
} from "../actions/translationActions";
import { storageGet, storageSet, storageRemove } from "../../utils/storage";
import { createUser } from "../../views/login/LoginAPI";

const STORAGE_KEY_PROFILE = "_lt-p";

export const profileMiddleware = ({ dispatch }) => (next) => (action) => {
    next(action);

    if (action.type === ACTION_PROFILE_CHECK_EXISTING) {
        const profile = storageGet(STORAGE_KEY_PROFILE);
        if (profile) {
            dispatch(profileSetAction(profile));
        }
    }

    if (action.type === ACTION_PROFILE_SET) {
        storageSet(STORAGE_KEY_PROFILE, action.payload);
        dispatch(translationFetchAction());
    }

    if (action.type === ACTION_PROFILE_CREATE) {
        (async () => {
            const created = await createUser(action.payload);
            if (created.username === action.payload) {
                dispatch(loginAction(action.payload));
            } else {
                dispatch(profileErrorAction("Profile creation failed"));
            }
        })();
    }

    if (action.type === ACTION_PROFILE_LOGOUT) {
        storageRemove(STORAGE_KEY_PROFILE);
        dispatch(translationSetAction([]));
    }
};
