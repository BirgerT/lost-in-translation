export const ACTION_TRANSLATION_SET = "translation:SET";
export const ACTION_TRANSLATION_FETCH = "translate:FETCH";
export const ACTION_TRANSLATION_STORE = "translation:STORE";
export const ACTION_TRANSLATION_CLEAR = "translation:CLEAR";

export const translationSetAction = (payload) => ({
    type: ACTION_TRANSLATION_SET,
    payload,
});

export const translationFetchAction = () => ({
    type: ACTION_TRANSLATION_FETCH,
});

export const translationStoreAction = (payload) => ({
    type: ACTION_TRANSLATION_STORE,
    payload,
});

export const translationClearAction = () => ({
    type: ACTION_TRANSLATION_CLEAR,
});
