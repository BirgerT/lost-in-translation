export const ACTION_PROFILE_CHECK_EXISTING = "profile:CHECK_EXISTING";
export const ACTION_PROFILE_SET = "profile:SET";
export const ACTION_PROFILE_CREATE = "profile:CREATE";
export const ACTION_PROFILE_LOGOUT = "profile:LOGOUT";
export const ACTION_PROFILE_ERROR = "profile:ERROR";

export const profileCheckExistingAction = () => ({
    type: ACTION_PROFILE_CHECK_EXISTING,
});

export const profileSetAction = (payload) => ({
    type: ACTION_PROFILE_SET,
    payload,
});

export const profileCreateAction = (payload) => ({
    type: ACTION_PROFILE_CREATE,
    payload,
});

export const profileLogoutAction = () => ({
    type: ACTION_PROFILE_LOGOUT,
});

export const profileErrorAction = (payload) => ({
    type: ACTION_PROFILE_ERROR,
    payload,
});
