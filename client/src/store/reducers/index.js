import { combineReducers } from "redux";
import { loginReducer } from "./loginReducer";
import { profileReducer } from "./profileReducer";
import { translationReducer } from "./translationReducer";

export const rootReducers = combineReducers({
    loginReducer,
    profileReducer,
    translationReducer,
});
