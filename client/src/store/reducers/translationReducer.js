import {
    ACTION_TRANSLATION_FETCH,
    ACTION_TRANSLATION_SET,
} from "../actions/translationActions";

const initialState = {
    translations: [],
    fetching: false,
};

export function translationReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_TRANSLATION_SET:
            return {
                translations: action.payload,
                fetching: false,
            };

        case ACTION_TRANSLATION_FETCH:
            return {
                translations: [],
                fetching: true,
            };

        default:
            return state;
    }
}
