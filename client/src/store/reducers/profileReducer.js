import {
    ACTION_PROFILE_SET,
    ACTION_PROFILE_LOGOUT,
    ACTION_PROFILE_ERROR,
} from "../actions/profileActions";

const initialState = {
    id: null,
    username: "",
    translations: [],
    error: "",
};

export function profileReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_PROFILE_SET:
            return {
                ...state,
                ...action.payload,
            };

        case ACTION_PROFILE_LOGOUT:
            return initialState;

        case ACTION_PROFILE_ERROR:
            return {
                ...initialState,
                error: action.payload,
            };

        default:
            return state;
    }
}
