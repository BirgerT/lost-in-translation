import {
    ACTION_LOGIN,
    ACTION_LOGIN_SUCCESS,
    ACTION_LOGIN_ERROR,
} from "../actions/loginActions";

const initialState = {
    fetching: false,
    error: "",
};

export function loginReducer(state = initialState, action) {
    switch (action.type) {
        case ACTION_LOGIN:
            return {
                fetching: true,
                error: "",
            };

        case ACTION_LOGIN_SUCCESS:
            return {
                fetching: false,
                error: "",
            };

        case ACTION_LOGIN_ERROR:
            return {
                fetching: false,
                error: action.payload,
            };

        default:
            return state;
    }
}
