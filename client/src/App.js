import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { profileCheckExistingAction } from "./store/actions/profileActions";

import Login from "./views/login/Login";
import Translate from "./views/translate/Translate";
import Profile from "./views/profile/Profile";
import NotFound from "./views/notfound/NotFound";

import "./App.scss";

function App() {
    const dispatch = useDispatch();
    dispatch(profileCheckExistingAction());

    return (
        <BrowserRouter>
            <div className="App">
                <Switch>
                    <Route exact path="/" component={Translate} />
                    <Route path="/login" component={Login} />
                    <Route path="/profile" component={Profile} />
                    <Route path="*" component={NotFound} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
